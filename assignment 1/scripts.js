// this function creates an array called 
// "provArray" with the provinces as the elements
// and populates the selectbox "cboProv" options 
function loadProvinces(){
    // initialize array provArray with province as elements
    var provArray = new Array ("Alberta", "British Columbia", "Saskatchewan",  "Manitoba", "Ontario", "Quebec", "Halifax", "Nova Scotia");

    // using for in, we create an option element for each
    // element in the array  
    for (element in provArray){
        // variable opt is used as a placeholder to assign
        // array element values into option elements
        var opt = document.createElement('option');
        // here we assign option element with array values
        opt.value = provArray[element];
        opt.innerHTML = provArray[element];
        // var select is use to identify the selectbox
        // "cboProv" where we want to make new options
        var select = document.getElementById('cboProv');
        // appendChild(opt) creates option element in selectbox 
        select.appendChild(opt);
    }
}

// this function validates if the form is complete
function validateForm() {
    // variables are created to get textbox values
    var name = document.getElementById("txtName").value;
    var email = document.getElementById("txtEmail").value;
    // variable is created to get a value for what is selected
    var prov = document.getElementById("cboProv").value;

    // to check if the form is filled out completely
    // we use else if so that when the form is not filled out 
    // it will send an alert to what is missing and focuses the 
    // required field
    // multiple if statements are not used, because then multiple
    // alerts will occur if several fields are not filled.
    if (name.length==0){
        alert("Please enter your name!");
        document.getElementById("txtName").focus();
    } else if (email.length==0){
        alert("Please enter your email!");
        document.getElementById("txtEmail").focus();
    } else if (prov==""){
        alert("Please choose a province!");
        document.getElementById("cboProv").focus();
    } else{
        alert("You have filled out the form successfully!");
    }
}